#include "types.h"
#include "user.h"

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        write(1, "correct number of arguments is 2.\n", sizeof("correct number of arguments is 2.\n"));
    }
    else if (strcmp(argv[1], "PATH") != 0)
    {
        write(1, "second argument should be PATH\n", sizeof("second argument should be PATH\n"));
    }
    else
    {
        set(argv[1], argv[2]);
        write(1, "Done.\n", sizeof("Done.\n"));
    }
    exit();
}
